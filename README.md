![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/lkwg82/gitlab-pipeline-dashboard.svg)

# Gitlab Pipeline Dashboard

This is a dashboard for [GitLab's CI](https://about.gitlab.com/gitlab-ci/) (continuous integration) pipelines! Best
viewed on a large display such as an TV.

[Try the demo dashboard.](https://lkwg82.gitlab.io/gitlab-pipeline-dashboard/)

This is my first release of the project and I am embarrassed by the sloppy code. So
please [report](https://gitlab.com/lkwg82/gitlab-pipeline-dashboard/issues) any issues you find! 🕵

## Quick start

for dev

```bash
./run.serve.sh
```

for deployment

```
./run.build.sh
```

skip Docker on macosx for faster builds
<
```
SKIP_DOCKER=1 ./run.build.sh
SKIP_DOCKER=1 ./run.serve.sh
```

## Hint

Currently it does not compile on macos without running in slow docker.

Delete `node_modules`, `pnpm-lock.yaml` and `.pnpm-store` and rerun `./run.serve.sh`. 
