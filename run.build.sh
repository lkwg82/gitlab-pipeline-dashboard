#!/bin/bash

set -euo pipefail

function build() {
  source __install.pnpm.sh

  pnpm install
  pnpm build

  if [[ -n ${CI:-} ]]; then
    mv public public-vue
    mv dist public
  fi
}

if [[ -n ${SKIP_DOCKER:-} ]]; then
  echo "> not running inside of docker"
  if [[ $(uname) == "Darwin" ]] && [[ $(uname -m) == "arm64" ]]; then
    echo "> running in x86_64 shell '$0'"
    export SKIP_DOCKER
    arch -x86_64 bash -c "$0"
  else
    build
  fi
elif [[ -z ${CI:-} ]] && [[ ! -f /.dockerenv ]]; then
  source __docker.sh
  run_docker ./run.build.sh
else
  build
fi
