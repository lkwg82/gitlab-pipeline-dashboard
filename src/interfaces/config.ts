export interface Config {
    url: string,
    token: string,
    groups: string,
    projects: string,
    group_namespaces: boolean,
    alpha_show_snow: boolean
    beta_order_by_failures_first: boolean,
    beta_show_only_protected_branches: boolean,
    beta_show_scheduled_builds: boolean,
    beta_show_history: boolean
}
