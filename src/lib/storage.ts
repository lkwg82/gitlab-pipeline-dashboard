function initStorage() {
    const appKey = 'gitlab-pipeline-dashboard'
    const localStorage = window.localStorage

    if (!localStorage) {
        throw new Error('storage has already been initialized')
    }

    const getStoreData = () => {
        return JSON.parse(localStorage.getItem(appKey) || '{}')
    }

    const setStoreData = (data: any) => {
        return localStorage.setItem(appKey, JSON.stringify({
            ...getStoreData(),
            ...data,
        }))
    }

    const get = (key: string) => {
        return getStoreData()[key]
    }

    const set = (key: string, value: any) => {
        return setStoreData({[key]: value})
    }

    return {get, set}
}

const storage = initStorage()

export default storage
