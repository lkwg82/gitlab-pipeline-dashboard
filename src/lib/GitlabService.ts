import {GitLabBranch, GitLabProject} from "@/interfaces";
import GitLab from "@/clients/gitlab";

class GitlabService {
    private gitlab: GitLab;
    constructor(gitlab: GitLab) {
        this.gitlab = gitlab;

    }

    public retrieve_protected_branches(project: GitLabProject): Promise<GitLabBranch[]> {
        // console.log("init " + project.name_with_namespace)
        return this.gitlab
            .getBranches(project.id)
            .then(
                branches => {
                    const protected_branches: GitLabBranch[] = []
                    for (const branch of branches) {
                        // console.log("  branch " + branch.name + " protected " + branch.protected)
                        if (branch.protected) {
                            protected_branches.push(branch)
                        }
                    }
                    return Promise.resolve(protected_branches)
                },
                _ => {
                    console.log("project has no branches " + project.name)
                    return Promise.resolve([])
                }
            )
    }
}

export default GitlabService
