#!/bin/bash


function run_docker() {
  local image=$(grep ^image .gitlab-ci.yml | cut -d\  -f2)


  if [[ $(uname -a) == *" arm64" ]]; then
    platform="linux/arm/v7"
  else
    platform="linux/amd64"
  fi

  echo "> run '$1' in docker  on platform '$platform' ..."
  docker run \
    --rm \
    --tty --interactive \
    --env "HOME=/tmp" \
    --entrypoint /bin/bash \
    --platform "${platform}" \
    --publish 8080:8080 \
    --volume "$PWD:/src" \
    --workdir /src \
    --user $(id -u) \
    "$image" "$1"

}
